const { MenuService } = require("../services");
const { success, error } = require("../network/response");

async function addMenu(req, res, next) {
  const menu = req.body;
  try {
    const result = await MenuService.addMenu({ menu });
    !result.error
      ? success(req, res, result.data, "menu created", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getMenus(req, res, next) {
  try {
    const result = await MenuService.getMenus();
    !result.error
      ? success(req, res, result.data, "menu listed", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function updateMenu(req, res, next) {
  const menu = req.body;
  const { id } = req.params;
  try {
    const result = await MenuService.updateMenu({ id, menu });
    !result.error
      ? success(req, res, result.data, "menu updated", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function activeMenu(req, res, next) {
  const menu = req.body;
  const { id } = req.params;
  try {
    const result = await MenuService.updateMenu({ id, menu });
    const message = menu.active
      ? "menu successfully activated"
      : "menu successfully disabled";
    !result.error
      ? success(req, res, result.data, message, 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function deleteMenu(req, res, next) {
  const { id } = req.params;
  try {
    const result = await MenuService.deleteMenu(id);
    !result.error
      ? success(req, res, result.data, "menu deleted", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  addMenu,
  getMenus,
  updateMenu,
  activeMenu,
  deleteMenu
};
