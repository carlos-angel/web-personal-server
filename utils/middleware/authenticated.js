const { auth } = require("../../config");
const { decodedToken } = require("../helpers/jwtHandler");
const boom = require("@hapi/boom");
const moment = require("moment");

const ensureAuth = (req, res, next) => {
  if (!req.headers.authorization) {
    next(boom.unauthorized());
  }

  const token = req.headers.authorization.replace(/['"]+/g, "");
  try {
    const payload = decodedToken(token);
    if (payload.exp <= moment.unix()) {
      next(boom.unauthorized("the token has expired"));
    }
    req.user = payload;
    next();
  } catch (error) {
    next(boom.unauthorized("token invalid"));
  }
};

module.exports = { ensureAuth };
