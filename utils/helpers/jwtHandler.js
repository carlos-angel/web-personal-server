const { auth } = require("../../config/index");
const jwt = require("jsonwebtoken");
const moment = require("moment");

function createAccessToken(user) {
  const payload = {
    id: user._id,
    name: user.name,
    lastname: user.lastname,
    role: user.role,
    createToken: moment().unix()
  };
  return jwt.sign(payload, auth.secret, {
    expiresIn: moment()
      .add(3, "hours")
      .unix()
  });
}

function createRefreshToken(user) {
  const payload = {
    id: user._id
  };
  return jwt.sign(payload, auth.secret, {
    expiresIn: moment()
      .add(30, "hours")
      .unix()
  });
}

function decodedToken(token) {
  return jwt.verify(token, auth.secret);
}

module.exports = { createAccessToken, createRefreshToken, decodedToken };
