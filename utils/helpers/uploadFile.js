const fs = require('fs');
const path = require('path');

function validateImage(file) {
  if (!file) {
    return false;
  }

  let filePath = file.path;
  let fileSplit = filePath.split('/');
  let fileName = fileSplit[2];

  let extSplit = fileName.split('.');
  let fileExt = extSplit[1];

  if (fileExt !== 'png' && fileExt !== 'jpg' && fileExt !== 'jpeg') {
    return false;
  }
  return fileName;
}

function pathImage(folder, name) {
  const filePath = `./uploads/${folder}/${name}`;
  const existAvatar = fs.existsSync(filePath);
  return existAvatar ? path.resolve(filePath) : false;
}

module.exports = {
  validateImage,
  pathImage,
};
