const { Newsletter } = require("../../models/index");

async function existEmail(email) {
  const existEmail = email;
  const user = await Newsletter.findOne({ email: existEmail.toLowerCase() });
  return user ? true : false;
}

module.exports = {
  existEmail
};
