const joi = require('joi');

const menuIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const menuTitleSchema = joi.string().min(3);
const menuUrlSchema = joi.string();
const menuOrderNameSchema = joi.number();
const menuActiveSchema = joi.boolean();

/**
 * @openapi
 * components:
 *   schemas:
 *     menuDto:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         title:
 *           type: string
 *         url:
 *           type: string
 *         order:
 *           type: number
 *           example: 1
 *         active:
 *           type: boolean
 *     createMenuDto:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *         url:
 *           type: string
 *         order:
 *           type: number
 *           example: 1
 *     updateMenuDto:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *         url:
 *           type: string
 *         order:
 *           type: number
 *           example: 1
 *         active:
 *           type: boolean
 */

const createMenuSchema = {
  title: menuTitleSchema.required(),
  order: menuOrderNameSchema.required(),
  url: menuUrlSchema.required(),
};

const updateMenuSchema = {
  title: menuTitleSchema,
  order: menuOrderNameSchema,
  url: menuUrlSchema,
  active: menuActiveSchema,
};

module.exports = {
  createMenuSchema,
  updateMenuSchema,
};
