const joi = require('joi');

const IdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const name = joi.string();
const review = joi.string();
const image = joi.string();
const jobTitle = joi.string();

/**
 * @openapi
 * components:
 *   schemas:
 *     reviewDto:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         name:
 *           type: string
 *         review:
 *           type: string
 *         jobTitle:
 *           type: string
 *         image:
 *           type: string
 *     createReviewDto:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *         review:
 *           type: string
 *         jobTitle:
 *           type: string
 *     updateReviewDto:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *         review:
 *           type: string
 *         jobTitle:
 *           type: string
 */

const create = {
  name: name.required(),
  review: review.required(),
  jobTitle: jobTitle.required(),
};

const update = {
  name: name,
  review: review,
  jobTitle: jobTitle,
};

const id = {
  id: IdSchema.required(),
};

module.exports = {
  id,
  create,
  update,
};
