const joi = require("joi");

const idSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const mongoIdSchema = {
  id: idSchema.required()
};

module.exports = {
  mongoIdSchema
};
