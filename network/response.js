const Boom = require("@hapi/boom");

const statusMessages = {
  200: "Done",
  201: "Created",
  400: "Invalid format",
  500: "Internal error"
};

const success = (req, res, data = "ok", message, statusCode = 200) => {
  res.status(statusCode).send({
    error: false,
    data: data,
    message: message
      ? message
      : statusMessages[statusCode]
      ? statusMessages[statusCode]
      : statusMessages[200]
  });
};

function error(req, res, boom) {
  const {
    output: { statusCode, payload }
  } = !boom.isBoom ? Boom.badImplementation() : boom;

  res.status(statusCode).json(payload);
}

const file = (req, res, filePath, statusCode = 200) => {
  res.status(statusCode).sendFile(filePath);
};

module.exports = {
  success,
  error,
  file
};
