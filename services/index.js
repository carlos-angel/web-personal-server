module.exports = {
  AuthService: require('./auth.service'),
  UserService: require('./user.service'),
  MenuService: require('./menu.service'),
  NewsletterService: require('./newsletter.service'),
  CourseService: require('./course.service'),
  PostService: require('./post.service'),
  CardInformationService: require('./card-information.service'),
  ReviewService: require('./review.service'),
};
