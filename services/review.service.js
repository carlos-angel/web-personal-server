const boom = require('@hapi/boom');

const { Review } = require('../models');
const { validateImage, pathImage } = require('../utils/helpers/uploadFile');

async function create({ review }) {
  const reviewCreated = new Review(review);
  await reviewCreated.save();

  if (!reviewCreated._id) {
    return { error: true, boom: boom.badRequest() };
  }

  return {
    error: false,
    data: reviewCreated._id,
  };
}

async function getAll() {
  const reviews = await Review.find();

  return {
    error: false,
    data: reviews ? reviews : [],
  };
}

async function update({ id, review }) {
  const reviewUpdated = await Review.findByIdAndUpdate(id, review);

  if (!reviewUpdated._id) {
    return { error: true, boom: boom.notFound('review not found') };
  }

  return {
    error: false,
    data: reviewUpdated._id,
  };
}

async function remove(id) {
  const reviewDeleted = await Review.findByIdAndDelete(id);

  if (!reviewDeleted) {
    return { error: true, boom: boom.notFound('review not found') };
  }

  return {
    error: false,
    data: reviewDeleted._id,
  };
}

async function getImage({ image }) {
  const path = pathImage('reviews', image);
  if (!path) {
    return { error: true, boom: boom.notFound('image not found') };
  }

  return {
    error: false,
    data: path,
  };
}

async function uploadImage({ id, image }) {
  const fileName = validateImage(image);
  if (!fileName) {
    return {
      error: true,
      boom: boom.badRequest(
        'Only images with jpg, jpeg and png extensions are allowed.',
      ),
    };
  }

  const review = await Review.findByIdAndUpdate(
    { _id: id },
    { image: fileName },
  );
  if (!review) {
    return { error: true, boom: boom.notFound() };
  }

  return {
    error: false,
    data: fileName,
  };
}

module.exports = {
  create,
  getAll,
  update,
  remove,
  getImage,
  uploadImage,
};
