const { Newsletter } = require("../models");
const { existEmail } = require("../utils/helpers/newsletterHandler");
const boom = require("@hapi/boom");

async function suscribeEmail({ email }) {
  const emailNewsletter = email.toLowerCase();
  if (await existEmail(emailNewsletter)) {
    return {
      error: true,
      boom: boom.badRequest(
        `the email ${emailNewsletter} is already registered.`
      )
    };
  }

  const newsletterAdded = new Newsletter({ email: emailNewsletter });
  newsletterAdded.save();
  if (!newsletterAdded._id) {
    return {
      error: true,
      boom: boom.badImplementation()
    };
  }

  return {
    error: false,
    data: newsletterAdded._id
  };
}

module.exports = {
  suscribeEmail
};
