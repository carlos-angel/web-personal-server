const { Course } = require('../models');
const { validateImage, pathImage } = require('../utils/helpers/uploadFile');
const boom = require('@hapi/boom');

async function courseExist(courseId) {
  const course = await Course.findOne({ courseId });
  return course ? true : false;
}

async function addCourse({ course }) {
  const courseRegisted = new Course(course);
  await courseRegisted.save();

  if (!courseRegisted._id) {
    return { error: true, boom: boom.badImplementation() };
  }

  return {
    error: false,
    data: courseRegisted._id,
  };
}

async function getCourses() {
  const courses = await Course.find().sort({ order: 'asc' });

  return {
    error: false,
    data: courses ? courses : [],
  };
}

async function updateCourse({ id, course }) {
  const courseUpdated = await Course.findByIdAndUpdate(id, course);

  if (!courseUpdated) {
    return { error: true, boom: boom.notFound('course not found') };
  }

  return {
    error: false,
    data: courseUpdated._id,
  };
}

async function deleteCourse({ id }) {
  const courseUpdated = await Course.findByIdAndDelete(id);

  if (!courseUpdated) {
    return { error: true, boom: boom.notFound('course not found') };
  }

  return {
    error: false,
    data: courseUpdated._id,
  };
}

async function uploadImage({ id, image }) {
  const fileName = validateImage(image);
  if (!fileName) {
    return {
      error: true,
      boom: boom.badRequest(
        'Only images with jpg, jpeg and png extensions are allowed.',
      ),
    };
  }

  const course = await Course.findByIdAndUpdate(
    { _id: id },
    { image: fileName },
  );
  if (!course) {
    return { error: true, boom: boom.notFound() };
  }

  return {
    error: false,
    data: fileName,
  };
}

async function getImageCourse({ image }) {
  const path = pathImage('courses', image);
  if (!path) {
    return { error: true, boom: boom.notFound('image not found') };
  }

  return {
    error: false,
    data: path,
  };
}

async function getCoursesOnOffer(limit = 4) {
  const courses = await Course.find({ offer: true }).limit(+limit);

  return {
    error: false,
    data: courses ? courses : [],
  };
}

module.exports = {
  addCourse,
  getCourses,
  updateCourse,
  deleteCourse,
  uploadImage,
  getImageCourse,
  getCoursesOnOffer,
};
