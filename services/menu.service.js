const { Menu } = require("../models");
const boom = require("@hapi/boom");

async function addMenu({ menu }) {
  const menuCreated = new Menu(menu);
  await menuCreated.save();

  return {
    error: false,
    data: menuCreated._id
  };
}

async function getMenus() {
  const menus = await Menu.find().sort({ order: "asc" });

  return {
    error: false,
    data: menus ? menus : []
  };
}

async function updateMenu({ id, menu }) {
  const menuUpdated = await Menu.findByIdAndUpdate(id, menu);

  if (!menuUpdated) {
    return { error: true, boom: boom.notFound("menu web not found") };
  }

  return {
    error: false,
    data: menuUpdated._id
  };
}

async function deleteMenu(id) {
  const menuDeleted = await Menu.findByIdAndDelete(id);

  if (!menuDeleted) {
    return { error: true, boom: boom.notFound("menu web not found") };
  }

  return {
    error: false,
    data: menuDeleted._id
  };
}

module.exports = {
  addMenu,
  getMenus,
  updateMenu,
  deleteMenu
};
