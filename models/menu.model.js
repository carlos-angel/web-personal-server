const { boolean } = require("joi");
const { Schema, model } = require("mongoose");

const MenuSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  order: {
    type: Number,
    required: true
  },
  active: {
    type: Boolean,
    default: true
  }
});

module.exports = model("Menu", MenuSchema);
