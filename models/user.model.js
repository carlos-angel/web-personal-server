const { Schema, model } = require("mongoose");

const UserSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
  lastname: {
    type: String,
    trim: true,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  avatar: {
    type: String,
    required: false
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    default: "ROLE_USER",
    required: true
  },
  active: {
    type: Boolean,
    default: false
  }
});

UserSchema.methods.toJSON = function() {
  let user = this.toObject();
  delete user.password;
  return user;
};

module.exports = model("User", UserSchema);
