module.exports = {
  User: require('./user.model'),
  Menu: require('./menu.model'),
  Newsletter: require('./newsletter.model'),
  Course: require('./course.model'),
  Post: require('./post.model'),
  CardInformation: require('./card-information.model'),
  Review: require('./review.model'),
};
