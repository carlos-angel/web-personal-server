const express = require('express');

const { CardInformationController } = require('../controllers');
const md_auth = require('../utils/middleware/authenticated');
const { CardInformationSchema } = require('../utils/schemas');
const validation = require('../utils/middleware/validationHandler');

function cardInformationApp(app) {
  const router = express.Router();
  app.use('/api/v1/card-information', router);

  /**
   * @openapi
   * /api/v1/card-information:
   *   get:
   *     tags:
   *       - card information
   *     summary: obtener tarjetas informativas
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/cardInformationDto'
   *                 message:
   *                   type: string
   *                   example: cards listed
   */
  router.get('/', CardInformationController.getTypeCardsInformation);

  /**
   * @openapi
   * /api/v1/card-information:
   *   post:
   *     tags:
   *       - card information
   *     summary: crear una tarjeta informativa
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/createCardInformationDto'
   *         required: true
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: card created
   */
  router.post(
    '/new',
    [md_auth.ensureAuth, validation(CardInformationSchema.create)],
    CardInformationController.addCardInformation,
  );

  /**
   * @openapi
   * /api/v1/card-information/{id}/update:
   *   put:
   *     tags:
   *       - card information
   *     summary: actualizar una tarjeta informativa
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID de la tarjeta informativa
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/updateCardInformationDto'
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: card updated
   */
  router.put(
    '/:id/update',
    [
      md_auth.ensureAuth,
      validation(CardInformationSchema.id, 'params'),
      validation(CardInformationSchema.update),
    ],
    CardInformationController.updateCardInformation,
  );

  /**
   * @openapi
   * /api/v1/card-information/{id}/delete:
   *   delete:
   *     tags:
   *       - card information
   *     summary: eliminar una tarjeta informativa
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID de la tarjeta informativa
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: card deleted
   */
  router.delete(
    '/:id/delete',
    [md_auth.ensureAuth, validation(CardInformationSchema.id, 'params')],
    CardInformationController.deleteCardInformation,
  );
}

module.exports = cardInformationApp;
