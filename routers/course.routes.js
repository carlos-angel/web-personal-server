const express = require('express');
const multipart = require('connect-multiparty');

const md_upload = multipart({ uploadDir: './uploads/courses' });
const { CourseController } = require('../controllers');
const md_auth = require('../utils/middleware/authenticated');
const { CourseSchema, MongoSchema } = require('../utils/schemas');
const validation = require('../utils/middleware/validationHandler');

function courseApp(app) {
  const router = express.Router();
  app.use('/api/v1/courses', router);

  /**
   * @openapi
   * /api/v1/courses:
   *   get:
   *     tags:
   *       - courses
   *     summary: obtener los cursos
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/courseDto'
   *                 message:
   *                   type: string
   *                   example: course listed
   */
  router.get('/', CourseController.getCourses);

  /**
   * @openapi
   * /api/v1/courses/new:
   *   post:
   *     tags:
   *       - courses
   *     summary: crear un curso
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/createCourseDto'
   *         required: true
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: course registered
   */
  router.post(
    '/new',
    [md_auth.ensureAuth, validation(CourseSchema.create)],
    CourseController.addCourse,
  );

  /**
   * @openapi
   * /api/v1/courses/{id}/update:
   *   put:
   *     tags:
   *       - courses
   *     summary: actualizar un curso
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del curso
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/updateCourseDto'
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: course updated
   */
  router.put(
    '/:id/update',
    [
      md_auth.ensureAuth,
      validation(MongoSchema.mongoIdSchema, 'params'),
      validation(CourseSchema.update),
    ],
    CourseController.updateCourse,
  );

  /**
   * @openapi
   * /api/v1/courses/{id}/delete:
   *   delete:
   *     tags:
   *       - courses
   *     summary: eliminar un curso
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del curso
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: course deleted
   */
  router.delete(
    '/:id/delete',
    [md_auth.ensureAuth, validation(MongoSchema.mongoIdSchema, 'params')],
    CourseController.deleteCourse,
  );

  /**
   * @openapi
   * /api/v1/courses/{id}/upload-image:
   *   put:
   *     tags:
   *       - courses
   *     summary: subir una imagen a un curso
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del curso
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/octet-stream:
   *             schema:
   *               type: string
   *               format: binary
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: I84SpIFoMLwsOoYi15kj3kuo.png
   *                 message:
   *                   type: string
   *                   example: upload image course success
   */
  router.put('/:id/upload-image', [md_auth.ensureAuth, md_upload], CourseController.uploadImage);

  /**
   * @openapi
   * /api/v1/courses/{image}/image:
   *   get:
   *     tags:
   *       - courses
   *     summary: obtener la imagen de un curso
   *     parameters:
   *        - name: image
   *          in: path
   *          description: nombre de la imagen
   *          required: true
   *          schema:
   *            type: string
   *            example: I84SpIFoMLwsOoYi15kj3kuo.png
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/octet-stream:
   *             schema:
   *               type: string
   *               format: binary
   */
  router.get('/:image/image', CourseController.getImageCourse);

  /**
   * @openapi
   * /api/v1/courses/{limit}/offer:
   *   get:
   *     tags:
   *       - courses
   *     summary: obtener los cursos que están en oferta
   *     parameters:
   *        - name: limit
   *          in: path
   *          description: cantidad de recursos
   *          required: true
   *          schema:
   *            type: number
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/courseDto'
   *                 message:
   *                   type: string
   *                   example: courses on offer
   */
  router.get(
    '/:limit/offer',
    validation(CourseSchema.limit, 'params'),
    CourseController.getCoursesOnOffer,
  );
}

module.exports = courseApp;
