const express = require('express');

const { MenuController } = require('../controllers');
const md_auth = require('../utils/middleware/authenticated');
const { MenuSchema } = require('../utils/schemas');
const validation = require('../utils/middleware/validationHandler');

function authApp(app) {
  const router = express.Router();

  app.use('/api/v1/menu', router);

  /**
   * @openapi
   * /api/v1/menu:
   *   post:
   *     tags:
   *       - menu
   *     summary: crear un enlace del menu
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/createMenuDto'
   *         required: true
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: menu created
   */
  router.post(
    '/new',
    [md_auth.ensureAuth, validation(MenuSchema.createMenuSchema)],
    MenuController.addMenu,
  );

  /**
   * @openapi
   * /api/v1/menu:
   *   get:
   *     tags:
   *       - menu
   *     summary: obtener datos para el menu del frontend
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/menuDto'
   *                 message:
   *                   type: string
   *                   example: menu listed
   */
  router.get('/', MenuController.getMenus);

  /**
   * @openapi
   * /api/v1/menu/{id}/update:
   *   put:
   *     tags:
   *       - menu
   *     summary: actualizar un enlace del menu
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del menu
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/updateMenuDto'
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: menu updated
   */
  router.put(
    '/:id/update',
    [md_auth.ensureAuth, validation(MenuSchema.updateMenuSchema)],
    MenuController.updateMenu,
  );

  /**
   * @openapi
   * /api/v1/menu/{id}/active:
   *   put:
   *     tags:
   *       - menu
   *     summary: activar o desactivar un enlace del menu
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del menu
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/updateMenuDto'
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: menu successfully activated/disabled
   */
  router.put(
    '/:id/activate',
    [md_auth.ensureAuth, validation(MenuSchema.updateMenuSchema)],
    MenuController.activeMenu,
  );

  /**
   * @openapi
   * /api/v1/menu/{id}/delete:
   *   delete:
   *     tags:
   *       - menu
   *     summary: eliminar un enlace del menu
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del menu
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: menu deleted
   */
  router.delete('/:id/delete', [md_auth.ensureAuth], MenuController.deleteMenu);
}

module.exports = authApp;
