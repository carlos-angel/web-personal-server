const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

// Metadata info about our API
const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'API - web personal',
      version: '1.0.0',
      description:
        'Administra los datos de tu sitio web personal, crea posts, reseñas y enlaces a cursos o sitios web de tu interés.',
    },
  },
  apis: ['routers/*.js', 'utils/schemas/*.js'],
};

// Docs en JSON format
const swaggerSpec = swaggerJSDoc(options);

/// Function to setup our docs
const swaggerDocs = (app) => {
  app.use('/api/v1/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
};

module.exports = { swaggerDocs };
