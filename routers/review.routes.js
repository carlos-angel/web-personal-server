const express = require('express');
const multipart = require('connect-multiparty');

const { ReviewController } = require('../controllers');
const md_auth = require('../utils/middleware/authenticated');
const { ReviewSchema } = require('../utils/schemas');
const validation = require('../utils/middleware/validationHandler');
const md_upload = multipart({ uploadDir: './uploads/reviews' });

function reviewApp(app) {
  const router = express.Router();
  app.use('/api/v1/review', router);

  /**
   * @openapi
   * /api/v1/review:
   *   get:
   *     tags:
   *       - review
   *     summary: obtener los reviews
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/reviewDto'
   *                 message:
   *                   type: string
   *                   example: review listed
   */
  router.get('/', ReviewController.getAll);

  /**
   * @openapi
   * /api/v1/review/new:
   *   post:
   *     tags:
   *       - review
   *     summary: crear un review
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/createReviewDto'
   *         required: true
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: review registered
   */
  router.post(
    '/new',
    [md_auth.ensureAuth, validation(ReviewSchema.create)],
    ReviewController.create,
  );

  /**
   * @openapi
   * /api/v1/review/{id}/update:
   *   put:
   *     tags:
   *       - review
   *     summary: actualizar un review
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del review
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/updateReviewDto'
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: review updated
   */
  router.put(
    '/:id/update',
    [md_auth.ensureAuth, validation(ReviewSchema.id, 'params'), validation(ReviewSchema.update)],
    ReviewController.update,
  );

  /**
   * @openapi
   * /api/v1/review/{id}/delete:
   *   delete:
   *     tags:
   *       - review
   *     summary: eliminar un review
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del review
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: review deleted
   */
  router.delete(
    '/:id/delete',
    [md_auth.ensureAuth, validation(ReviewSchema.id, 'params')],
    ReviewController.remove,
  );

  /**
   * @openapi
   * /api/v1/review/{id}/upload-image:
   *   put:
   *     tags:
   *       - review
   *     summary: subir una imagen a un review
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del review
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/octet-stream:
   *             schema:
   *               type: string
   *               format: binary
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: I84SpIFoMLwsOoYi15kj3kuo.png
   *                 message:
   *                   type: string
   *                   example: upload image review success
   */
  router.put('/:id/upload-image', [md_auth.ensureAuth, md_upload], ReviewController.uploadImage);

  /**
   * @openapi
   * /api/v1/review/{image}/image:
   *   get:
   *     tags:
   *       - review
   *     summary: obtener la imagen de un review
   *     parameters:
   *        - name: image
   *          in: path
   *          description: nombre de la imagen
   *          required: true
   *          schema:
   *            type: string
   *            example: I84SpIFoMLwsOoYi15kj3kuo.png
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/octet-stream:
   *             schema:
   *               type: string
   *               format: binary
   */
  router.get('/:image/image', ReviewController.getImage);
}

module.exports = reviewApp;
