module.exports = {
  authApp: require('./auth.routes'),
  userApp: require('./user.routes'),
  menuApp: require('./menu.routes'),
  newsletterApp: require('./newsletter.routes'),
  courseApp: require('./course.routes'),
  postApp: require('./post.routes'),
  cardInformationApp: require('./card-information.routes'),
  reviewApp: require('./review.routes'),
};
