const express = require('express');
const cors = require('cors');
const routes = require('./routers');
const notFoundHandler = require('./utils/middleware/notFoundHandler');
const { logErrors, wrapError, errorHandler } = require('./utils/middleware/errorHandlers');
const { swaggerDocs } = require('./routers/swagger');

const app = express();

/** middleware */
app.use(express.json());
app.use(cors());

/** routes */
routes.authApp(app);
routes.userApp(app);
routes.menuApp(app);
routes.newsletterApp(app);
routes.courseApp(app);
routes.postApp(app);
routes.cardInformationApp(app);
routes.reviewApp(app);

app.get('/', (req, res) => res.redirect('/api/v1/docs'));
swaggerDocs(app);

/** 404 */
app.use(notFoundHandler);

/** errors */
app.use(logErrors);
app.use(wrapError);
app.use(errorHandler);

module.exports = app;
